#include QMK_KEYBOARD_H

#ifdef PROTOCOL_LUFA
  #include "lufa.h"
  #include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif

#include "raw_hid.h"

extern uint8_t is_master;

#define _COLE 0
#define _LOWER 1
#define _RAISE 2
#define _LEFTPAD 3

enum custom_keycodes {
  COLE = SAFE_RANGE,
  LOWER,
  LEFTPAD,
  RAISE,
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* COLE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ESC  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  `   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   F  |   P  |   B  |                    |   J  |   L  |   U  |   Y  |   :  |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Ctrl |   A  |   R  |   S  |   T  |   G  |-------.    ,-------|   M  |   N  |   E  |   I  |   O  |  '   |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |LShift|   Z  |   X  |   C  |   D  |   V  |-------|    |-------|   K  |   H  |   ,  |   .  |   /  |  =   |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LEFT | LAlt |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

 [_COLE] = LAYOUT( \
  KC_ESC,         KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                        KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_GRV,  \
  KC_TAB,         KC_Q,   KC_W,    KC_F,    KC_P,    KC_B,                        KC_J,    KC_L,    KC_U,    KC_Y,    KC_SCLN, KC_MINS, \
  CTL_T(KC_BSPC), KC_A,   KC_R,    KC_S,    KC_T,    KC_G,                        KC_M,    KC_N,    KC_E,    KC_I,    KC_O,    KC_QUOT, \
  KC_LSFT,        KC_Z,   KC_X,    KC_C,    KC_D,    KC_V,    KC_LBRC,  KC_RBRC,  KC_K,    KC_H,    KC_COMM, KC_DOT,  KC_SLSH, KC_EQL,  \
							 LOWER,   KC_LALT, LEFTPAD, KC_SPC,   KC_ENT,   RAISE,   KC_BSPC, KC_RGUI \
),
/* LOWER
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |Sleep |      |      |      |      |      |                    |      |      |      |   *  |   ^  |   _  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |   7  |   8  |   9  |   &  |   |  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   !  |   @  |   #  |   $  |   %  |-------.    ,-------|  +   |   4  |   5  |   6  |   (  |   )  |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|   0  |   1  |   2  |   3  |   {  |   }  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LCTRL |LOWER | /Space  /       \PEnter\  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
[_LOWER] = LAYOUT( \
  KC_SLEP, _______, _______, _______, _______, _______,                   _______, _______, _______, KC_PAST, KC_CIRC, KC_UNDS,\
  _______, _______, _______, _______, _______, _______,                   _______, KC_P7,   KC_P8,   KC_P9,   KC_AMPR, KC_PIPE, \
  KC_GRV, KC_EXLM, KC_AT,    KC_HASH, KC_DLR,  KC_PERC,                   KC_PPLS, KC_P4,   KC_P5,   KC_P6,   KC_LPRN, KC_RPRN, \
  _______, _______, _______, _______, _______, _______, _______, _______, KC_P0,   KC_P1,   KC_P2,   KC_P3,   KC_LCBR, KC_RCBR, \
                             _______, _______, _______, _______, KC_PENT,  _______, _______, _______\
),
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |RESET | Swap |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   `  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |-------.    ,-------|      | Left | Down |  Up  |Right |      |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |  F7  |  F8  |  F9  | F10  | F11  | F12  |-------|    |-------|   +  |   -  |   =  |   [  |   ]  |   \  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LCTRL |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */

[_RAISE] = LAYOUT( \
  RESET,   CG_TOGG, _______, _______, _______, _______,                     _______, _______, _______, _______, _______, _______, \
  KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                        KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    _______, \
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                       XXXXXXX, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, XXXXXXX, \
  KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,   _______, _______,  KC_PLUS, KC_MINS, KC_EQL,  KC_LBRC, KC_RBRC, KC_BSLS, \
                             _______, _______, _______,  _______, _______,  _______, _______, _______ \
),
/* Leftypad
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |Sleep | ⏮  |  ⏭  | Vol- | Vol+ | 🔇  |                    | SLEEP |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |backSP|   /  |  7   |   8  |   9  |   -  |                    |   `  |   !  |   @  |   #  |   $  |   %  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |   *  |   4  |   5  |   6  |   +  |-------.    ,-------|   ^  |   &  |   *  |   (  |   )  |   -  |
 * |------+------+------+------+------+------| PEnt  |    |       |------+------+------+------+------+------|
 * |      |   0  |   1  |  2   |  3   |   .  |-------|    |-------|      |   _  |   +  |   {  |   }  |   |  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   |  ⏯  |      |LOWER | /       /       \      \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
[_LEFTPAD] = LAYOUT( \
  KC_MPLY, KC_MPRV, KC_MNXT, KC_VOLD, KC_VOLU, KC_MUTE,                     KC_SLEP, _______, _______, _______, _______, _______, \
  KC_BSPC, KC_PSLS, KC_P7,   KC_P8,   KC_P9,   KC_PMNS,                     KC_GRV,  KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, \
  _______, KC_PAST, KC_P4,   KC_P5,   KC_P6,   KC_PPLS,                     KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_TILD, \
  _______, KC_P0,   KC_P1,   KC_P2,   KC_P3,   KC_PDOT,  KC_PENT, _______,  XXXXXXX, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE, \
                             _______, _______, _______,  _______, _______,  _______, _______, _______ \
),
};


#ifdef OLED_DRIVER_ENABLE
const char *read_layer_state(void);
const char *read_logo(void);
const char *read_mode_icon(bool swap);

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_keyboard_master())
    return OLED_ROTATION_180;  // flips the display 180 degrees if offhand
  return rotation;
}


//
// START HID OLED
//

bool is_hid_connected = false; // Flag indicating if we have a PC connection yet
uint8_t screen_max_count = 0;  // Number of info screens we can scroll through (set by connecting node script)
uint8_t screen_show_index = 0; // Current index of the info screen we are displaying
uint8_t screen_data_buffer[512] =  {0}; // Screen buffer
uint8_t serial_slave_screen_buffer[512];
bool screen_buffer_ready = false;
int screen_data_index = 0; // Current index into the screen_data_buffer that we should write to

void raw_hid_send_screen_index(void) {
	// Send the current info screen index to the connected node script so that it can pass back the new data
	uint8_t send_data[RAW_EPSIZE] = {0};
	send_data[0] = screen_show_index + 1; // Add one so that we can distinguish it from a null byte
	raw_hid_send(send_data, sizeof(send_data));
}

void raw_hid_receive(uint8_t *data, uint8_t length) {
	// PC connected, so set the flag to show a message on the master display
	is_hid_connected = true;
	if (length > 1 && data[0] == 1) {
		// New connection so restart screen_data_buffer
		screen_data_index = 0;

		// The second byte is the number of info screens the connected node script allows us to scroll through
		screen_max_count = data[1];
		if (screen_show_index >= screen_max_count) {
			screen_show_index = 0;
		}

		// Tell the connection which info screen we want to look at initially
		raw_hid_send_screen_index();
		return;
	}
	// First byte if not 1 tells us how many pages to expect
	if (length > 1  && data[0] > 1) {
		// Copy only the data we need
		memcpy((char*)&screen_data_buffer[screen_data_index * (length-2)], data+1, (length-2));
		screen_data_index++;
		// Reached max pages
		if (screen_data_index == data[0]-2) {
			// Reset the buffer index back
			screen_data_index = 0;
			// Reset buffer
			memset((char*)&serial_slave_screen_buffer[0], ' ', sizeof(serial_slave_screen_buffer));
			// Set buffer
			memcpy((char*)&serial_slave_screen_buffer[0], screen_data_buffer, sizeof(screen_data_buffer));
			// Render buffer
			screen_buffer_ready = true;
		}
  	}
	return;
}
// Ignore, not necessary
char layer_state_str[24];
const char *write_layer(void) {
  // Print the layer name for the current layer
  switch (biton32(layer_state))
  {
  case _COLE:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Colemak");
    break;
  case _LOWER:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Right Numpad");
    break;
  case _RAISE:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Arrow Keys");
    break;
  case _LEFTPAD:	
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Left Numpad");
    break;
  default:
    snprintf(layer_state_str, sizeof(layer_state_str), "Layer: Id-%ld", layer_state);
  }

  return layer_state_str;
}
char hid_info_str[24];
const char *write_hid(void) {
  snprintf(hid_info_str, sizeof(hid_info_str), "%s", is_hid_connected ? "connected." : " ");
  return hid_info_str;
}
// End ignore

void oled_task_user(void) {
    // slave oled doesn't seem to update
	if (!is_keyboard_master()) {
        oled_write_ln(write_layer(), false);
		oled_write_ln("", false);
		oled_write_ln(write_hid(), false);
	}
	else {
		if (screen_buffer_ready) {
			// clear buffer (doesn't seem to work?)
			oled_write(" ", false);
			// write compiled buffer
			oled_write_raw((char*)serial_slave_screen_buffer, sizeof(serial_slave_screen_buffer));
			screen_buffer_ready = false;
		} else {
			// Otherwise we just draw the logo
			oled_write("", false);
		}
	}
}

//
// END HID OLED
//

#endif

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case COLE:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_COLE);
      }
      return false;
      break;
    case LOWER:
      if (record->event.pressed) {
        layer_on(_LOWER);
      } else {
        layer_off(_LOWER);
      }
      return false;
      break;
    case RAISE:
      if (record->event.pressed) {
        layer_on(_RAISE);
      } else {
        layer_off(_RAISE);
      }
      return false;
      break;
	case LEFTPAD:
      if (record->event.pressed) {
        layer_on(_LEFTPAD);
      } else {
        layer_off(_LEFTPAD);
      }
      return false;
      break;
  }
  return true;
}